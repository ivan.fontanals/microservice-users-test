package com.idaltec.msa.users;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

public interface UserService {

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public String getUser();

}
