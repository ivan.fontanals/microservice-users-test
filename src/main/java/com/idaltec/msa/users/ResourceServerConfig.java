package com.idaltec.msa.users;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override // [3]
    public void configure(HttpSecurity http) throws Exception {
         // @formatter:off
         http.addFilterBefore(new CorsFilter(), BasicAuthenticationFilter.class)
         // Just for laughs, apply OAuth protection to only 2 resources
         .requestMatchers().antMatchers("/","/user").and()
         .authorizeRequests()
         .anyRequest().access("#oauth2.hasScope('read')"); //[4]
         // @formatter:on
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
         resources.resourceId("resourceId");
    }

}