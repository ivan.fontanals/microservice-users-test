FROM java:8
VOLUME /tmp
ENV environment=cloud
ADD target/*.jar app.jar
ENTRYPOINT java -Dspring.profiles.active=$environment -jar /app.jar
EXPOSE 8080